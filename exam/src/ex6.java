import java.awt.*;

class PC {
    Monitor theMonitor;
    Case theCase;
    Motherboard theMotherboard;


    public PC(Monitor theMonitor, Case theCase, Motherboard theMotherboard) {
        this.theMonitor = theMonitor;
        this.theCase = theCase;
        this.theMotherboard = theMotherboard;
    }

    public Monitor getTheMonitor() {
        return theMonitor;
    }

    public Case getTheCase() {
        return theCase;
    }

    public Motherboard getTheMotherboard() {
        return theMotherboard;
    }

    public void pressPowerButton() {
        System.out.println("Power button Pressed");
    }
}

class Monitor {
    String model;
    String manufacturer;
    int size;
    Resolution resolution;

    public Monitor(String model, String manufacturer, int size, Resolution resolution) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.size = size;
        this.resolution = resolution;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }
}

class Case {
    String model;
    String manufacturer;
    String powerSupply;
    Dimension dimensions;

    public Case(String model, String manufacturer, String powerSupply) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.powerSupply = powerSupply;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        this.powerSupply = powerSupply;
    }

    public Dimension getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;
    }
}

class Motherboard {
    String model;
    String manufacturer;
    int cardSloth;
    int bios;

    public Motherboard(String model, String manufacturer, int cardSloth, int bios, String s) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.cardSloth = cardSloth;
        this.bios = bios;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getCardSloth() {
        return cardSloth;
    }

    public void setCardSloth(int cardSloth) {
        this.cardSloth = cardSloth;
    }

    public int getBios() {
        return bios;
    }

    public void setBios(int bios) {
        this.bios = bios;
    }
}

class Dimensions {
    int width;
    int height;
    int depth;

    public Dimensions(int width, int height, int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
}

class Resolution {
    int a;
    int b;

    public Resolution(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}


public class ex6 {
    public static void main(String[] args) {
        Dimensions dimensions = new Dimensions (20, 20, 5);
        Case theCase = new Case("220B", "Dell", "240");

        Monitor theMonitor = new Monitor("27inch Beast", "Acer", 27, new Resolution(2540, 1440));

        Motherboard theMotherboard = new Motherboard("BJ-200", "Asus", 4, 6, "v2.44");

        PC thePC = new PC (theCase, theMonitor, theMotherboard);

    }

}
