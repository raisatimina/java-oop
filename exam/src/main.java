import java.sql.SQLOutput;

class Floor {
    double width;
    double length;


    public Floor(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth (double newWidth) {
        this.width = newWidth;
        if (newWidth < 0){
            this.width = 0;
        }
    }

    public void setLength (double newLength) {
        this.length = newLength;
        if (newLength < 0){
            this.length = 0;
        }
    }

    public double getArea() {
        return (this.width*this.length);
    }
}

class Carpet {
    double cost;

    public Carpet(double cost) {
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double newCost) {
        this.cost = newCost;
        if (newCost < 0) {
            this.cost = 0;
        }
    }
}

class Calculator {
    Floor floor;
    Carpet carpet;

    public Calculator(Floor floor, Carpet carpet) {
        this.floor = floor;
        this.carpet = carpet;
    }

    public double getTotalCost() {
        return (floor.getArea())*carpet.getCost();
    }
}

public class main {
    public static void main(String[] args) {
        Carpet carpet = new Carpet(3.5);
        Floor floor = new Floor(2.75, 4.0);
        Calculator calculator = new Calculator(floor, carpet);
        System.out.println("total = " + calculator.getTotalCost());
        carpet = new Carpet(1.5);
        floor = new Floor(5.4, 4.5);
        calculator = new Calculator(floor, carpet);
        System.out.println("total = " + calculator.getTotalCost());    }

}
