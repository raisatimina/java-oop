
abstract class Bank {
    double balance;

    abstract double getBalance();
}

class BankA extends Bank {

    public BankA(double balance) {
        this.balance = balance;
    }

    @Override
    double getBalance() {
        return balance;
    }
}

class BankB extends Bank {

    public BankB(double balance) {
        this.balance = balance;
    }

    @Override
    double getBalance() {
        return 150;
    }
}

class BankC extends Bank {

    public BankC(double balance) {
        this.balance = balance;
    }

    @Override
    double getBalance() {
        return 200;
    }
}

public class ex5 {
    public static void main(String[] args) {
        BankA a = new BankA(100);
        System.out.println("balance is: " + a.getBalance());

        BankB b = new BankB(150);
        System.out.println("balance is: " + b.getBalance());

        BankC c = new BankC(200);
        System.out.println("balance is: " + c.getBalance());
    }
}
