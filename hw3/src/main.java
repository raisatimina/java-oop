class Rectangle {
    double width;
    double length;


    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }


    public double getWidth() {
        return width;
    }

    public void setWidth(double newWidth) {
        this.width = newWidth;
        if (newWidth < 0) {
            this.width = 0;
        }
    }

    public double getLength() {
        return length;
    }

    public void setLength(double newLength) {
        this.length = newLength;
        if (newLength < 0) {
            this.length = 0;
        }
    }

    public double getArea(){
        return(this.width*this.length);
    }
}

class Cuboid extends Rectangle {
    double height;

    public Cuboid(double width, double length, double height) {
        super(width, length);
        this.height = height;
    }

    public double getHeight(){
        return height;
    }

    public double getVolume() {
        return (getArea()*this.height);
    }
}

public class main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(5, 10);
        System.out.println("rectangle.width= " + rectangle.getWidth());
        System.out.println("rectangle.length= " + rectangle.getLength());
        System.out.println("rectangle.area= " + rectangle.getArea());
        Cuboid cuboid = new Cuboid(5, 10, 5);
        System.out.println("cuboid.width= " + cuboid.getWidth());
        System.out.println("cuboid.length= " + cuboid.getLength());
        System.out.println("cuboid.are= " + cuboid.getArea());
        System.out.println("cuboid.height= " + cuboid.getHeight());
        System.out.println("cuboid.volume= " + cuboid.getVolume());
    }
}
