import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;

public class main {
    //Assignment
    //1- Create a Class User with the fields(int id, string name, java.time.LocalDate birthdate)
    //2- Create a list of 5 users with test data.
    //3- Create a method name it as sortListBytype(int type)
    //4- based on the type sort them by id, by name and birthdate
    public static void main(String[] args) {
        User a = new User(2, "cla", LocalDate.of(3020, 8, 12));
        User b = new User(2, "csdf", LocalDate.of(3020, 3, 12));
        User v = new User(33, "asdf", LocalDate.of(4020, 8, 12));
        User d = new User(555, "asdf", LocalDate.of(5020, 8, 12));
        User e = new User(77, "feedrf", LocalDate.of(6060, 8, 12));

        UserList ul = new UserList();

        ul.addUSer(a);
        ul.addUSer(b);
        ul.addUSer(v);
        ul.addUSer(d);
        ul.addUSer(e);



        ul.sortByType(1);
        ul.printUL();

        ul.sortByType(2);
        ul.printUL();

        ul.sortByType(3);
        ul.printUL();



    }
}

class User {
    int id;
    String name;
    LocalDate bday;
    public User(int id, String name, LocalDate bday) {
        this.id = id;
        this.name = name;
        this.bday = bday;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", bday=" + bday +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBday() {
        return bday;
    }

    public void setBday(LocalDate bday) {
        this.bday = bday;
    }

}

class UserList {
    ArrayList<User> list = new ArrayList<>();


    public void addUSer(User user) {
        this.list.add(user);
    }

    public void sortByType(int x) {
        if (x == 1) {
            this.list.sort(Comparator.comparing(User::getBday));
        } else if (x == 2) {
            this.list.sort(Comparator.comparing(User::getId));
        } else if (x == 3) {
            this.list.sort(Comparator.comparing(User::getName));
        }

    }

    public void printUL() {
        for (User user : this.list) {
            System.out.println(user.toString());
        }
        System.out.println("==========================");
    }
}