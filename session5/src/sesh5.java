class Dancer {
    public void dance(){
        System.out.println(("Adelante"));
    }
}

class ElectricBoogieDancer extends Dancer {

    @Override
    public void dance() {
        System.out.println("Creepin");
    }
}

class Breakdancer extends Dancer {

    @Override
    public void dance() {
        System.out.println("Backflip");
    }
}

public class sesh5 {
    public static void main(String[] args) {
        Dancer dancer = new Dancer();
        ElectricBoogieDancer boogie = new ElectricBoogieDancer();
        Breakdancer breakdancer = new Breakdancer();

        Dancer[] Dancer = new Dancer[3];
        Dancer[0] = dancer;
        Dancer[1] = boogie;
        Dancer[2] = breakdancer;

        for (int i = 0; i < 3; i++) {
            Dancer[i].dance();
        }
        }
    }

