//exercise 1(abstraction)

abstract class Shape {
    double perimeter;
    double area;

    public Shape(double perimeter, double area) {
        this.perimeter = perimeter;
        this.area = area;
    }

    public Shape() {

    }

    abstract double getPerimeter();

    abstract double getArea();

    abstract void setArea(double newArea);

    abstract void setPerimeter(double newPerimeter);
}

class Square extends Shape {
    int side;

    public Square(int side) {
        this.side = side;
        setPerimeter(this.side);
        setArea(this.side);
    }

    @Override
    double getPerimeter() {
        return this.perimeter;
    }

    @Override
    double getArea() {
        return this.area;
    }

    @Override
    void setArea(double newArea) {
        newArea = this.side * this.side;
        this.area = newArea;
    }

    @Override
    void setPerimeter(double newPerimeter) {
        newPerimeter = 4 * this.side;
        this.perimeter = newPerimeter;

    }


}

class Circle extends Shape {
    int radius;

    public Circle(int radius) {
        this.radius = radius;
        setPerimeter(this.radius);
        setArea(this.radius);
    }

    @Override
    double getPerimeter() {
        return perimeter;
    }

    @Override
    double getArea() {
        return area;
    }

    @Override
    void setArea(double newArea) {
        newArea = 3.14 * (this.radius * this.radius);
        this.area = newArea;
    }

    @Override
    void setPerimeter(double newPerimeter) {
        newPerimeter = 2 * 3.14 * this.radius;
        this.perimeter = newPerimeter;
    }
}

//exercise 1(no abstraction)

class Circle2 {
    int radius;

    public Circle2(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getArea(){
        return (int) (3.14 * (this.radius * this.radius));
    }

    public int getPerimeter(){
        return (int)(2 * 3.14 * this.radius);
    }


}

class Square2 {
    int side;

    public Square2(int side) {
        this.side = side;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public int getArea(){
        return (int) (this.side * this.side);
    }

    public int getPerimeter(){
        return (int)(4 * this.side);
    }
}

//exercise2

abstract class Animals {

    abstract void Sound();

}

class Cats extends Animals {

    @Override
    void Sound() {
        System.out.println("Meow");
    }
}

class Dogs extends Animals {

    @Override
    void Sound() {
        System.out.println("Woof");
    }
}

//exercise 3

abstract class Marks {
    int percentage;

    abstract int getPercentage();
}

class A extends Marks {

    public A(int a1, int a2, int a3) {
        this.percentage = (a1 + a2 + a3) / 3;

    }

    @Override
    int getPercentage() {
        return percentage;
    }
}

class B extends Marks {

    public B(int b1, int b2, int b3, int b4) {
        this.percentage = (b1 + b2 + b3 + b4) / 4;

    }

    @Override
    int getPercentage() {
        return percentage;
    }
}

public class main {


    public static void main(String[] args) {

        System.out.println("Answer to question 1. abstraction allows for a better organization\n " +
                "of the code by ability to apply repetitive methods onto multiple objects.\n" +
                "It also allows to avoid the repetition of the code for the subclasses");
//        Square square = new Square(5);
////        a.setPerimeter();
//        System.out.println("perimeter of a square is " + square.getPerimeter());
//        System.out.println("area of a square is " + square.getArea());
//
//        System.out.println();
//
//        Circle circle = new Circle(5);
//        System.out.printf("perimeter of a circle is %.1f\n" , circle.getPerimeter());
//        System.out.printf("area of a circle is %.1f\n" , circle.getArea());
//
//        System.out.println("\n");

//        Circle2 test = new Circle2(4);
//        System.out.println(test.getArea() + " " + test.getPerimeter());
//
//        Square2 test2 = new Square2(6);
//        System.out.println(test2.getArea() + " " + test2.getPerimeter());

        //Exercise 2

        Cats cat = new Cats();
        cat.Sound();

        Dogs dog = new Dogs();
        dog.Sound();

        //exercise 3

        A studentA = new A(50, 30, 100);
        System.out.println("average A" +studentA.getPercentage());

        B studentB = new B(100, 100, 90, 100);
        System.out.println("average B" + studentB.getPercentage());


    }
}
