import java.io.*;
import java.util.ArrayList;

public class main {
//1-Create a folder in your project and name it resource
//2- Create a method that takes a list<String> as an argument and name it as "writeToFile"
//3- Create a class name it as Address that consists of (buildingNo, StreetName, cityName, provinceName and postalCode )
//4- in the main method create 5 fake addresses and add them into a list.
//5- Call method writeToFile and save the values in the file
//6- Create another method name it as "ReadFromFile" that returns a list of Address
//7- call the method ReadFromFile() and show the address in the output.
//
//** Try to find a way to insert data into the file, in a way that is feasible to take out the value and make the object again !!!!


    public static void main(String[] args) {


        Address a = new Address("1", "First", "One", "Blah", "hr4h4h");
        Address b = new Address("2", "Secondary", "Two", "Blah", "h4h3h2h");
        Address c = new Address("3", "Third", "Tree", "Blah", "hr4h4h");
        Address d = new Address("4", "Fours", "Four", "Blah", "h4h3h2h");
        Address e = new Address("5", "Fifth", "Five", "Blah", "h4h3h2h");
//        System.out.println("Done creating tests");


//        ArrayList<String> list = new ArrayList<>();
//        list.add(a.toString());
//        list.add(b.toString());
//        list.add(c.toString());
//        list.add(d.toString());
//        list.add(e.toString());
        ArrayList<Address> addarrlist = new ArrayList<>();
        addarrlist.add(a);
        addarrlist.add(b);
        AddressList adList = new AddressList(addarrlist);

        try
            // File writing portion
            // Output stream opens a file
            // Try with (resource) {}
            // Anything in the parenthiesis is guaranteed to be properly closed.
                (
                        FileOutputStream outputStream = new FileOutputStream("test.txt");
                        // Object out manages serialization for us and we write it to the outputstream
                        ObjectOutputStream oos = new ObjectOutputStream(outputStream)
                ) {
            oos.writeObject(adList);


            // Here we initialize our file reader
            // And out object "deserializer"
            FileInputStream fs = new FileInputStream("test.txt");
            ObjectInputStream oi = new ObjectInputStream(fs);


            // We read from oi, into an object and DownCast it to Our initial class AddressList
            AddressList result = (AddressList) oi.readObject();



            System.out.println("=======");
            // Now we can manipulate our object as usual
            System.out.println(result.toString());


        } catch (IOException | ClassNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
//        writeToDisk(list, filename);
    }


}

class AddressList implements Serializable {
    ArrayList<Address> list = new ArrayList<>();

    public AddressList(ArrayList<Address> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "AddressList{" +
                "list=" + list +
                '}';
    }
}

class Address implements Serializable {
    String buildingNo;
    String StreetName;
    String cityName;
    String provinceName;
    String postalCode;

    public Address(String buildingNo, String streetName, String cityName, String provinceName, String postalCode) {
        this.buildingNo = buildingNo;
        StreetName = streetName;
        this.cityName = cityName;
        this.provinceName = provinceName;
        this.postalCode = postalCode;
    }

    public String getBuildingNo() {
        return buildingNo;
    }

    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    public String getStreetName() {
        return StreetName;
    }

    public void setStreetName(String streetName) {
        StreetName = streetName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "buildingNo='" + buildingNo + '\'' +
                ", StreetName='" + StreetName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
