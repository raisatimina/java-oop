import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class main {
    public static void main(String[] args) {
        List<Message> listOfMessages = new ArrayList<>();
        EmailMessage email = new EmailMessage(
                new EmailUser("Judy", "Foster", new Address("Main Street", 1), "a.b@g.com"),
                new EmailUser("Betty", "Beans", new Address("second street", 2), "v.r@g.com"),
                "This is one email");

        SmsMessage smsMessage = new SmsMessage(
                new SmsUSer("Judy", "Foster", new Address("Main Street", 1), "122123"),
                new SmsUSer("Betty", "Beans", new Address("second street", 2), "122123"),
                "This is one sms");


        listOfMessages.add(email);
        listOfMessages.add(smsMessage);

        SendEmail a = new SendEmail();
        a.sendMessage(listOfMessages.set(0, email));


        SendSms b = new SendSms();
        b.sendMessage(listOfMessages.set(1, smsMessage));

        System.out.println(email.getBody());
        System.out.println(smsMessage.getBody());
    }
}