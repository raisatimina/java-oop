public class SmsUSer extends User{
    String phoneNumber;

    public SmsUSer(String firstName, String lastName, Address address, String phoneNumber) {
        super(firstName, lastName, address);
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    public void setFirstName(String firstName) {
        super.setFirstName(firstName);
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    @Override
    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    @Override
    public Address getAddress() {
        return super.getAddress();
    }

    @Override
    public void setAddress(Address address) {
        super.setAddress(address);
    }
}