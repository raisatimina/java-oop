import java.util.regex.Matcher;
import java.util.regex.Pattern;

class EmailMessage extends Message{

    public EmailMessage(User receiver, User sender, String body) {
        super(receiver, sender, body);
    }


    @Override
    public User getReceiver() {
        return super.getReceiver();
    }

    @Override
    public void setReceiver(User receiver) {
        super.setReceiver(receiver);
    }

    @Override
    public User getSender() {
        return super.getSender();
    }

    @Override
    public void setSender(User sender) {
        super.setSender(sender);
    }

    @Override
    public String getBody() {
        return super.getBody();
    }

    @Override
    public void setBody(String body) {
        super.setBody(body);
    }
}
