import java.io.*;

public class SendSms  implements ISendInfo {

    @Override
    public boolean validateMessage(User sender, User receiver, String body) {
        if (body == "" || body.length() < 10) {
            throw new IllegalArgumentException("Please type in your information");
        }
        return false;
    }

    @Override
    public void sendMessage(Message message) {
        try (FileOutputStream outputStream = new FileOutputStream("sms.txt");
             ObjectOutputStream oos = new ObjectOutputStream(outputStream)) {
            oos.writeObject(new SmsMessage(message.getReceiver(), message.getSender(), message.getBody()));

            FileInputStream fs = new FileInputStream("sms.txt");
            ObjectInputStream oi = new ObjectInputStream(fs);


        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }
}

