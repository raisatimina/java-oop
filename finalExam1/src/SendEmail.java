import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendEmail implements ISendInfo{

    @Override
    public boolean validateMessage(User sender, User receiver, String body) {
        if (body == "") {
            throw new IllegalArgumentException("Please type in your information");
        }

//        Pattern correctEmail = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
//        Matcher matcher = correctEmail.matcher(sender.getEmailAddress);
//        return matcher.find();
          return false;
        }

    @Override
    public void sendMessage(Message message) {
        try (FileOutputStream outputStream = new FileOutputStream("email.txt");
             ObjectOutputStream oos = new ObjectOutputStream(outputStream)) {
            oos.writeObject(new EmailMessage(message.getReceiver(), message.getSender(), message.getBody()));

            FileInputStream fs = new FileInputStream("email.txt");
            ObjectInputStream oi = new ObjectInputStream(fs);

        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }

        }
    }

