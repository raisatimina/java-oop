import java.util.ArrayList;

public class NurseResults {
    // check record
    double temperature;
    double bloodPressure;
    int weight;
    int height;
    ArrayList<String> symptoms;

    public NurseResults(double temperature, double bloodPressure, int weight, int height, ArrayList<String> symptoms) {
        this.temperature = temperature;
        this.bloodPressure = bloodPressure;
        this.weight = weight;
        this.height = height;
        this.symptoms = symptoms;
    }

    public NurseResults(){

    }


    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public double getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(int bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public ArrayList<String> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(ArrayList<String> symptoms) {
        this.symptoms = symptoms;
    }
}

