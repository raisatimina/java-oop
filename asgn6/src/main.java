
class Payroll {
    Employee[] employees;

    public void paySalary(){
        //whatever you want it to mean, he didnt specify
        // for loop, on the array of emp, that calls the function pay or salary or whatever
        for (int i = 0; i < employees.length; i++) {
            employees[i].salary();
        }
    }
}

class Employee {
    String name;
    String ssn;


    public Employee(String name, String ssn) {
        this.name = name;
        this.ssn = ssn;
    }


    public double salary(){
        return salary();
    }

    public String toString(){
        return this.name + ", " + this.ssn;
    }
}

class CommissionEmployee extends Employee{

    double sales;
    double commission;

    public CommissionEmployee(String name, String ssn, double sales, double commission) {
        super(name, ssn);
        this.sales = sales;
        this.commission = commission;
    }

    public double getSales() {
        return sales;
    }

    public void setSales(double sales) {
        this.sales = sales;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }


    @Override
    public double salary() {
        return (this.sales+this.commission);
    }

    @Override
    public String toString() {
        return "name = " + name + "\n" +
                "ssn = " + ssn + "\n" +
                "sales = " + sales + "\n" +
                "commission =" + commission + "\n" +
                "salary = " + salary()  + "\nDone\n\n\n";
    }
}

class HourlyEmployee extends Employee {
    double wage;
    double hours;

    public HourlyEmployee(String name, String ssn, double wage, double hours) {
        super(name, ssn);
        this.wage = wage;
        this.hours = hours;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    @Override
    public double salary() {
        return (this.wage * this.hours);
    }

    @Override
    public String toString() {
        return "name = " + name + "\n" +
                "ssn = " + ssn + "\n" +
                "wage = " + wage + "\n" +
                "hours = " + hours + "\n" +
                "salary = " + salary() + "\nDone\n\n\n";
    }
}

class SalariedEmployee extends Employee{
    double basicSalary;

    public SalariedEmployee(String name, String ssn, double basicSalary) {
        super(name, ssn);
        this.basicSalary = basicSalary;
    }


    @Override
    public double salary() {
        return basicSalary;
    }

    @Override
    public String toString() {
        return "name = " + name + "\n" +
                "ssn = " + ssn + "\n" +
                "salary = " + salary() + "\nDone\n\n\n";
    }

}

public class main {
    public static void main(String[] args) {
        CommissionEmployee e1 = new CommissionEmployee("Dude", "magic", 12,12);
        System.out.print(e1);
        HourlyEmployee e2 = new HourlyEmployee("Dudette", "123456", 12, 12);
        System.out.print(e2);
        SalariedEmployee e3 = new SalariedEmployee("Dudetine", "564393", 40);
        System.out.print(e3);
        System.out.println("q2: We need to override the hashcodes and equal methods because in the even though each of " +
                "the extending classes, in this case, receive a salary, they all have their salary consistent of different variables. " +
                "Therefore must be adjusted for each class.");
        System.out.println("q3: an abstract class comes more handy when using an inheritance and/or private classes. Abstract " +
                "also allows for more convenience in the future when changes are required");
        System.out.println("q4: lets take current exercise as an example. Employee has a name and a ssn, downcast to Salaried Employee who owns" +
                "both variables mentioned above and basic salary on top of that");
    }



}
