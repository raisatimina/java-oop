import java.io.*;

public class main {
    public static void main(String[] args) {
//        Create a folder in your project and name it resource
//2- Create a method that takes a list<String> as an argument and name it as "writeToFile"
//3- Create a class name it as Address that consists of (buildingNo, StreetName, cityName, provinceName and postalCode )
//4- in the main method create 5 fake addresses and add them into a list.
//5- Call method writeToFile and save the values in the file
//6- Create another method name it as "ReadFromFile" that returns a list of Address
//7- call the method ReadFromFile() and show the address in the output.
//
//** Try to find a way to insert data into the file, in a way that is feasible to take out the value and make the object again !!!!

        File file = new File("Named.txt");
        try {

            FileOutputStream fileInputStream = new FileOutputStream(file);
//            fileInputStream.w
//            fileInputStream.write("asdf".getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class test implements Serializable {
    int i = 2;
}

class ObjectUitls {
    // Serialization
    // Save object into a file.
    public static void writeObject(Person obj, File file) throws IOException {
        try (
//                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))
        ) {
            oos.writeObject(obj);
            oos.flush();
        }
    }

    // Deserialization
    // Get object from a file.
    public static Person readObject(File file) throws IOException, ClassNotFoundException {
        Person result = null;
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            result = (Person) ois.readObject();
        }
        return result;
    }
}

class Person implements Serializable {

    // optional, if missing, JVM will create it.
    // better declare as a version control.
    private static final long serialVersionUID = 1L;

    private String name;
    private int age;


    public Person(String name, int age) {
        this.name = name;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}